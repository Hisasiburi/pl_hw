#lang plai-typed 

; type variable
(define-type TE
             [numTE]
             [arrowRE (param : TE) (result : TE)]
             [guessTE])

(define-type Type
             [numT]
             [arrowT (param : Type) (result : Type)]
             [varT (is : (boxof (Option Type)))])

(define-type (Option 'alpha)
             [none]
             [some (v : 'alpha)])

(define (unify! t1 t2 expr)
  (type-case Type t1
             [varT (is1) (type-case (Option Type) (unbox is1)
                                    [some (t3) (unify! t3 t2 expr)]                  ; If T is set to t3, unify t3 and t2
                                    [none () (local [(define t3 (resolve t2))]
                                               (if (eq? t1 t3)                       ; If t2 is already equivalent to T, succeed
                                                 (values)                             
                                                 (if (occurs? t1 t3)                 ; If t2 contains T, then fail
                                                   (begin                            
                                                     (set-box! is1 (some t3))        ; Otherwise, set T to t2 and succeed
                                                     (values)))))])]
             [else (type-case Type t2
                              [varT (is2) (unify! t2 t1 expr)]                       ; If t2 is a type variable T, then unify T and t1
                              [numT () (type-case Type t1
                                                  [numT () (values)]                 ; If t1 and t2 are both num or bool, succeed.
                                                  [else (type-error expr t1 t2)])]   ;; fail
                              [arrowT (a2 b2)
                                      (type-case Type t1
                                                 [arrowT (a1 b1)                     ; If t1 is (t3 ->t4) and t2 is (t5 -> t6), then
                                                         (begin (unify! a1 a2 expr)  ;;; unify t3 with t5
                                                                (unify! b1 b2 expr))];;; unify t4 with t6
                                                 [else (type-error expr t1 t2)])])]));; fail

(define (resolve t)
  (type-case Type t
             [varT (is) (type-case (Option Type) (unbox is)
                                   [none () t]
                                   [some (t2) (resolve t2)])]
             [else t]))

; check whether 't' contains 'r' or not
(define (occurs? r t)
  (type-case Type t)
  [numT () false]
  [arrowT (a b) (or (occurs? r a) (occurs? r b))]
  [varT (is) (or (eq? r t)
                 (type-case (Option Type) (unbox is)
                            [none () false]
                            [some (t2) (occurs? r t2)]))])

(define (typecheck : (TIFAE TypeEnv -> Type)
                   (lambda (tifae env)
                     (type-case TIFAE tifae
                                [num (n) (numT)]
                                [add (l r)
                                     (type-case Type (typecheck l env)
                                                [numT ()
                                                      (type-case Type (typecheck r env)
                                                                 [numT () (numT)]
                                                                 [else (type-error r "num")])]
                                                [else (type-error l "num")])]
                                [sub (l r)
                                     (type-case Type (typecheck l env)
                                                [numT ()
                                                      (type-case Type (typecheck r env)
                                                                 [numT () (numT)]
                                                                 [else (type-error r "num")])]
                                                [else (type-error l "num")])]
                                [id (name) (get-type name env)]
                                [fun (name te body)
                                     (local [(define param-type (parse-type te))]
                                       (validtype param-type env)
                                       (arrowT param-type
                                               (typecheck body
                                                          (aBind name param-type env))))]
                                [app (fn arg)
                                     (type-case Type (typecheck fn env)
                                                [arrowT (param-type result-type)
                                                        (if (equal? param-type
                                                                    (typecheck arg env))
                                                          result-type
                                                          (type-error arg
                                                                      (to-string param-type)))]
                                                [else (type-error fn "function")])]
