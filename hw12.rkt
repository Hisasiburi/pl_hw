#lang plai-typed

(define (type-error e msg)
  (error 'typecheck (string-append
                     "no type: "
                     (string-append
                      (to-string e)
                      (string-append " not "
                                     msg)))))

(define-type EXPR
  [num (n : number)]
  [bool (b : boolean)]
  [add (lhs : EXPR) (rhs : EXPR)]
  [sub (lhs : EXPR) (rhs : EXPR)]
  [equ (lhs : EXPR) (rhs : EXPR)]
  [id (name : symbol)]
  [fun (param : symbol) (paramty : TE) (body : EXPR)]
  [app (fun-expr : EXPR) (arg-expr : EXPR)]
  [ifthenelse (test-expr : EXPR) (then-expr : EXPR) (else-expr : EXPR)]
  [rec (name : symbol) (ty : TE) (named-expr : EXPR) (body : EXPR)]
  [with-type (name : symbol)
             (var1-name : symbol) (var1-ty : TE)
             (var2-name : symbol) (var2-ty : TE)
             (body-expr : EXPR)]
  [cases (name : symbol)
         (dispatch-expr : EXPR)
         (var1-name : symbol) (bind1-name : symbol) (rhs1-expr : EXPR)
         (var2-name : symbol) (bind2-name : symbol) (rhs2-expr : EXPR)]
  [tfun (name : symbol) (expr : EXPR)]
  [tapp (body : EXPR) (type : TE)])

(define-type TE
  [numTE]
  [boolTE]
  [arrowTE (param : TE) (result : TE)]
  [polyTE (forall : symbol) (body : TE)]
  [idTE (name : symbol)]
  [tvTE (name : symbol)])

(define-type Type
  [numT]
  [boolT]
  [arrowT (param : Type) (result : Type)]
  [polyT (forall : symbol) (body : Type)]
  [idT (name : symbol)]
  [tvT (name : symbol)])

(define-type DefrdSub
  [mtSub]
  [aSub (id : symbol) (val : EXPR-Value) (rest : DefrdSub)])

(define-type EXPR-Value
  [numV (n : number)]
  [boolV (b : boolean)]
  [closureV (param : symbol) (body : EXPR) (ds : DefrdSub)]
  [variantV (right? : boolean) (val : EXPR-Value)]
  [constructorV (right? : boolean)])
  
(define-type TypeEnv
  [mtEnv]
  [aBind (name : symbol) (type : Type) (rest : TypeEnv)]
  [tBind (name : symbol)
         (var1-name : symbol) (var1-type : Type)
         (var2-name : symbol) (var2-type : Type)
         (rest : TypeEnv)]
  [tvBind (name : symbol) (type : Type) (rest : TypeEnv)])

(define parse-type : (TE -> Type)
  (lambda (te)
    (type-case TE te
      [numTE () (numT)]
      [boolTE () (boolT)]
      [arrowTE (param result) (arrowT (parse-type param) (parse-type result))]
      [polyTE (forall body) (polyT forall (parse-type body))]
      [idTE (name) (idT name)]
      [tvTE (name) (tvT name)])))

(define get-type : (symbol TypeEnv -> Type)
  (lambda (name-to-find env)
    (type-case TypeEnv env
      [mtEnv () (error 'get-type "free variable, so no type")]
      [aBind (name ty rest)
             (if (symbol=? name name-to-find)
                 ty
                 (get-type name-to-find rest))]
      [tBind (name var1-name var1-ty var2-name var2-ty rest)
             (get-type name-to-find rest)]
      [tvBind (name type rest)
              (get-type name-to-find rest)])))
    
(define find-type-id : (symbol TypeEnv -> TypeEnv)
  (lambda (name-to-find env)
    (type-case TypeEnv env
      [mtEnv () (error 'find-type-id "free type name, so no type")]
      [aBind (name type rest)
             (find-type-id name-to-find rest)]
      [tBind (name var1-name var1-ty var2-name var2-ty rest)
             (if (symbol=? name name-to-find)
                 env
                 (find-type-id name-to-find rest))]
      [tvBind (name type rest)
              (if (symbol=? name name-to-find)
                  env
                  (find-type-id name-to-find rest))])))
    
(define validtype : (Type TypeEnv -> TypeEnv)
  (lambda (ty env)
    (type-case Type ty
      [numT () (mtEnv)]
      [boolT () (mtEnv)]
      [arrowT (a b) (begin (validtype a env)
                           (validtype b env))]
      [polyT (a b) (validtype b (tvBind a (tvT a) env))]
      [idT (name) (find-type-id name env)]
      [tvT (name) (find-type-id name env)])))

(define lookup-type : (symbol TypeEnv -> Type)
  (lambda (name env)
    (type-case TypeEnv env
      [mtEnv () (error 'lookup-type "free type name, so no type")]
      [aBind (name type rest)
             (lookup-type name rest)]
      [tBind (name v1-name v1-ty v2-name v2-type rest)
             (lookup-type name rest)]
      [tvBind (bindname boundtype rest)
              (if (symbol=? name bindname)
                  boundtype
                  (lookup-type name rest))])))

(define subtitute-type : (Type TypeEnv -> Type)
  (lambda (ty env)
    (type-case Type ty
      [numT () ty]
      [boolT () ty]
      [arrowT (a b) (arrowT (subtitute-type a env) (subtitute-type b env))]
      [polyT (a b) (polyT a (subtitute-type b (tvBind a (tvT a) env))]
      [idT (name) ty]
      [tvT (name) (lookup-type name env)])))
             
(define typecheck : (EXPR TypeEnv -> Type)
  (lambda (expr env)
    (type-case EXPR expr
      [num (n) (numT)]
      [bool (b) (boolT)]
      [add (l r) (type-case Type (typecheck l env)
                   [numT () (type-case Type (typecheck r env)
                              [numT () (numT)]
                              [else (type-error r "num")])]
                   [else (type-error l "num")])]
      [sub (l r) (type-case Type (typecheck l env)
                   [numT () (type-case Type (typecheck r env)
                              [numT () (numT)]
                              [else (type-error r "num")])]
                   [else (type-error l "num")])]
      [equ (l r) (type-case Type (typecheck l env)
                   [numT () (type-case Type (typecheck r env)
                              [numT () (numT)]
                              [else (type-error r "num")])]
                   [else (type-error l "num")])]
      [id (s) (get-type s env)]
      [fun (p pt b) (local [(define param-type (parse-type pt))] ; should be checked well-definedness
                      (begin
                        (validtype param-type env)
                        (arrowT param-type
                              (typecheck b (aBind p param-type env)))))]
      [app (f a) (type-case Type (typecheck f env)
                   [arrowT (p r) (if (equal? p (typecheck a env))
                                     r
                                     (type-error a (to-string p)))]
                   [else (type-error f "function")])]
      [ifthenelse (test then else-exp) 
                  (type-case Type (typecheck test env)
                    [boolT () (local [(define then-ty (typecheck then env))]
                                (if (equal? then-ty (typecheck else-exp env))
                                    then-ty
                                    (type-error else-exp (to-string then-ty))))]
                    [else (type-error test "boolean")])]
      [rec (name te def body)
        (local [(define ty (parse-type te))
                (define new-env (aBind name ty env))]
          (if (equal? ty (typecheck def new-env))
              (typecheck body new-env)
              (type-error def (to-string ty))))]
      [with-type (t-name v1-name v1-te v2-name v2-te body)
                 (local [(define v1-ty (parse-type v1-te))
                         (define v2-ty (parse-type v2-te))
                         (define new-env (tBind t-name
                                                v1-name v1-ty
                                                v2-name v2-ty
                                                env))]
                   (begin
                     (validtype v1-ty new-env)
                     (validtype v2-ty new-env)
                     (typecheck body (aBind v1-name
                                            (arrowT v1-ty (idT t-name))
                                            (aBind v2-name
                                                   (arrowT v2-ty (idT t-name))
                                                   new-env)))))]
      [cases (t-name dispatch 
                   v1-name v1-param v1-expr
                   v2-name v2-param v2-expr)
        (local [(define t (find-type-id t-name env))]
          (if (and (symbol=? v1-name (tBind-var1-name t))
                   (symbol=? v2-name (tBind-var2-name t)))
              (type-case Type (typecheck dispatch env)
                [idT (name) (if (symbol=? t-name name)
                                (local [(define v1-ty (typecheck v1-expr (aBind v1-param
                                                                                (tBind-var1-type t)
                                                                                env)))
                                        (define v2-ty (typecheck v2-expr (aBind v2-param
                                                                                (tBind-var2-type t)
                                                                                env)))]
                                  (if (equal? v1-ty v2-ty)
                                      v1-ty
                                      (type-error v2-expr (to-string v1-ty))))
                                (type-error dispatch (to-string t-name)))]
                [else (type-error dispatch (to-string (to-string t-name)))])
              (type-error expr "matching variant names")))]
      [tfun (param tf-expr)
            (polyT param (typecheck tf-expr (tvBind param (tvT param) env)))]
      [tapp (tf te)
            (local ([define ty (parse-type te)])
              (begin
                (validtype ty env)
                (type-case Type (typecheck tf env)
                  [polyT (forall body-ty) (subtitute-type body-ty (tvBind forall ty env))]
                  [else (type-error tf "type function")])))])))
            